# VElixir

VElixir is a simple to use primitive 3D graphics library. It offers great performance, and ease of use. VElixir was inspired by VPython, which makes basic 3D graphics in python trivial. Now it's just as easy to make 3D graphical visualizations in Elixir.

**Curently only tested on Mac! Community testing, and contributions to help with multi-platform support are welcome!**

Current object types:
- Sphere
- Cylinder
- Cone
- Arrow

VElixir is easy to extend. New meshes and objects can be added with ease.

Current features:
- Transformation matrices are automatically inferred from simple data attributes like `pos`, `radius`, and `dir`
- Transformation matrices can still be manually specified instead
- Built in Vector math, and basic operators (+-\*/) are overrided to work with vectors
- Any GenServer can be used as a render source
- Meshes and Objects can be easily added to VElixir
- Mesh data is stored in Vertex Array Objects on the gpu for fast performance

Current limitations:
- Meshes must be uniformly colored, and textures are not supported
- Meshes cannot be modified after creation, only transformed

## Installation

Add `velixir` and `enm` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:enm, github: "basho/enm"},
    {:velixir, "~> 0.0.7"}
  ]
end
```

## Example

You can run this example by entering `mix demo grav`

```elixir
defmodule Grav do
  use VElixir.GenServer

  def start_link(opts \\ []) do
    GenServer.start_link(__MODULE__, nil, opts)
  end

  # def g, do: 6.67408e-11

  def init(_) do
    {:ok, _} = VElixir.start_link

    state = %{
      objects: Enum.map(1..100, fn _ ->
        %{vtype: Sphere, pos: V.random(0.85), radius: 0.05, color: Color.random, mass: 5, vel: V.new}
      end),
      # arrow: %{vtype: Arrow, pos: V.new},
      dt: 0, t: 0,
      render: [:objects]
    }

    {:ok, state}
  end

  def handle_cast({:update, dt, t}, state) do
    state = %{state|dt: dt, t: t}

    objects = state.objects
      |> VElixir.Misc.worker_map(10, fn obj -> update(obj, state) end)

    {:noreply, %{state|objects: objects}}#, arrow: Map.put(state.arrow, :dir, hd(objects).pos)}}
  end

  def update(obj, state) do
    obj
      |> calc_force(state.objects)
      |> calc_kinematics(state.dt)
  end

  def calc_force(obj, objects) do
    Map.put(obj, :net_force, Enum.reduce(objects, V.new, fn other_obj, net_force ->
      if obj == other_obj do
        net_force #make sure not to count force from ourselves
      else
        r = other_obj.pos - obj.pos
        net_force + (norm(r) * ((1/length(objects)) * obj.mass * other_obj.mass / :math.pow(mag(r), 1)))
      end
    end))
  end

  def calc_kinematics(obj, dt) do
    accel = obj.net_force / obj.mass
    obj
      |> Map.put(:vel, obj.vel + accel*dt)
      |> Map.put(:pos, obj.pos + obj.vel*dt)
  end
end
```

Here is a basic template to start from to build a visualization.
Replace all occurences of `Template` with your own module name.

Use `mix <your_module_name>` to run.

```elixir
defmodule Template do
  use VElixir.GenServer

  def start_link(args, opts \\ []) do
    GenServer.start_link(__MODULE__, args, opts)
  end

  def init(_args) do
    {:ok, _} = VElixir.start_link

    state = %{
      objects: [], # declare your objects here
      render: [:objects]
    }

    {:ok, state}
  end

  def handle_cast({:update, _dt, _t}, state) do
    objects = Enum.map(state.objects, fn obj ->
      # modify your objects here
      obj
    end)
    {:noreply, %{state|objects: objects}}
  end
end

defmodule Mix.Tasks.Template do
  def run(args) do
    {:ok, _} = Template.start_link(args)
    # block until the program ends
    receive do
      {:stop, _reason} -> nil
    end
  end
end
```
