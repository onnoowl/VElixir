#version 150 core

//global
uniform mediump vec4 infPointLightPos;
uniform lowp float infPointLightIntensity;
uniform lowp float ambientMin;
uniform highp mat4 proj_matrix;
uniform lowp mat4 translation_matrix; //moves the objects into a better position for the camera

//per object
uniform highp mat4 obj_transform;
uniform lowp vec4 obj_color;

//per vertex input
in mediump vec3 vertex_position;
in lowp vec3 vertex_normal;

//output to fragment shader
out lowp vec4 color;

void main() {
  mat4 transformation = translation_matrix * obj_transform;
  mediump vec4 real_position = transformation * vec4(vertex_position, 1.0);
  gl_Position = proj_matrix * real_position;

  lowp vec4 weird_normal = transpose(inverse(transformation)) * vec4(vertex_normal, 1.0);
  lowp vec4 trans_normal = vec4(normalize(weird_normal.xyz), 1);
  // color = abs(vec4(vertex_normal, 0)); //useful for debugging, shows normals
  // color = abs(trans_normal);
  float intensity = max(
    infPointLightIntensity * clamp(dot(trans_normal, normalize(infPointLightPos - real_position)), 0,1),
    ambientMin
  );
  color = obj_color * intensity;
}
