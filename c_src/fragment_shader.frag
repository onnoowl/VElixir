#version 150 core

in lowp vec4 color;
out lowp vec4 frag_colour;

void main () {
  frag_colour = color;
}
