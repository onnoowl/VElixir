defmodule VElixir.Mixfile do
  use Mix.Project

  def project do
    [app: :velixir,
     version: "0.0.7",
     elixir: "~> 1.2",
     compilers: [:elixir_make, :elixir, :app],
     build_embedded: Mix.env == :prod,
     start_permanent: Mix.env == :prod,
     description: description,
     package: package,
     deps: deps]
  end

  def application do
    [applications: [:logger, :enm, :msgpax]]
  end

  defp description do
    """
    VElixir is a simple to use primitive 3D graphics library. It offers great performance, and ease of use. VElixir was inspired by VPython, which makes basic 3D graphics in python trivial. Now it's just as easy to make 3D graphical visualizations in Elixir.
    """
  end

  defp package do
    [
      files: ["lib", "c_src/cnano.cpp", "c_src/fragment_shader.frag", "c_src/vertex_shader.vert", "README.md", "mix.exs", "Makefile"],
      maintainers: ["Stuart Hunt"],
      licenses: ["Apache 2.0"],
      links: %{"GitLab" => "https://gitlab.com/onnoowl/VElixir"}
    ]
  end

  defp deps do
    [{:enm, github: "basho/enm"},
    {:elixir_make, "~> 0.1.0"},
    {:msgpax, "~> 0.7"}]
  end
end
