cnano: c_src/cnano.cpp c_src/msgpack-c/libmsgpackc.a c_src/glfw/src/libglfw3.a c_src/nanomsg/build/libnanomsg.dylib#c_src/glew/src/thingy.a
	g++ \
	-O3 \
	-lnanomsg -Lc_src/nanomsg/build -I c_src/nanomsg/src \
	-Lc_src/msgpack-c -I c_src/msgpack-c/include/ \
	-Lc_src/glfw/src -I c_src/glfw/include -lglfw3  \
	-framework OpenGL -framework Cocoa -framework CoreVideo -framework IOKit \
	-o cnano \
	c_src/cnano.cpp

c_src/msgpack-c/libmsgpackc.a: c_src/msgpack-c/include/msgpack.hpp
	cd c_src/msgpack-c && \
	cmake . && \
	make
c_src/msgpack-c/include/msgpack.hpp:
	git clone https://github.com/msgpack/msgpack-c.git c_src/msgpack-c

c_src/glfw/src/libglfw3.a: c_src/glfw/include/GLFW/glfw3.h
	cd c_src/glfw && \
	cmake . && \
	make
c_src/glfw/include/GLFW/glfw3.h:
	git clone https://github.com/glfw/glfw.git c_src/glfw

c_src/nanomsg/build/libnanomsg.dylib: c_src/nanomsg/src/nn.h
	mkdir c_src/nanomsg/build && \
	cd c_src/nanomsg/build && \
	cmake .. && \
	cmake --build . && \
	ctest -C Debug . && \
	cmake --build . --target install
c_src/nanomsg/src/nn.h:
	git clone https://github.com/nanomsg/nanomsg.git c_src/nanomsg

clean:
	rm -f cnano
