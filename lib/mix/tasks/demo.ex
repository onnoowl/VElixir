defmodule Mix.Tasks.Demo do
  def run([demo_name]) do
    demo_module = case demo_name do
      "rgb" -> Rgb
      "grav" -> Grav
      "orbit" -> Orbit
    end
    {:ok, _pid} = demo_module.start_link
    receive do
      {:stop, _reason} -> nil
    end
  end
end
