defmodule Rgb do
  use VElixir.GenServer

  def start_link(opts \\ []) do
    GenServer.start_link(__MODULE__, nil, opts)
  end

  def init(_) do
    {:ok, _} = VElixir.start_link

    r = 0.3
    x = 2*r/:math.sqrt(2)
    state = %{
      objects: [
        %{vtype: Cone, pos: V.new(-x, x, 0), radius: r, color: Color.new(1, 0, 0), height: r},
        %{vtype: Cylinder, pos: V.new(0, 0, 0),  radius: r, color: Color.new(0, 1, 0), height: r},
        %{vtype: Sphere, pos: V.new(x, -x, 0), radius: r, color: Color.new(0, 0, 1)},
      ],
      render: [:objects]
    }

    {:ok, state}
  end

  def handle_cast({:update, dt, t}, state) do
    dy = :math.cos(0.5*:math.pi*t)
    rotatePoint = Enum.at(state.objects, 1).pos
    objects = Enum.map(state.objects, fn obj ->
      obj
        |> Map.put(:pos, obj.pos + V.new(0, dt*dy, 0))
        |> Map.put(:post_transform, Transform.rotate(t*:math.pi*0.5, V.new(1, 1, 0), rotatePoint))
    end)
    {:noreply, %{state|objects: objects}}
  end
end
