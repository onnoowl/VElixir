defmodule VElixir.V do
  alias VElixir.V

  defstruct [:x, :y, :z]

  def x, do: new(1, 0, 0) #the x unit vector
  def y, do: new(0, 1, 0) #the y unit vector
  def z, do: new(0, 0, 1) #the z unit vector

  def new(x, y, z), do: %V{x: x, y: y, z: z}
  def new, do: new(0, 0, 0)
  def random(bound \\ 1), do: V.map_elements(V.new, fn _ -> 2*bound*:rand.uniform - bound end)

  def map_elements(v, fun) do
    Enum.reduce [:x, :y, :z], v, fn e, v ->
      Map.put(v, e, fun.(e))
    end
  end
  def pointwise(a, fun) do
    map_elements(%V{}, fn e -> fun.(Map.fetch!(a, e)) end)
  end
  def pointwise(a, b, fun) do
    map_elements(%V{}, fn e -> fun.(Map.fetch!(a, e), Map.fetch!(b, e)) end)
  end

  def add(a, b), do: vec_or_scalar(a, b, &+/2)
  def sub(a, b), do: vec_or_scalar(a, b, &-/2)
  def mult(a, b), do: vec_or_scalar(a, b, &*/2)
  def div(a, b), do: vec_or_scalar(a, b, &//2)
  def vec_or_scalar(a, b, fun) do
    cond do
      is_number(a) && is_number(b) -> fun.(a, b)
      is_number(b) -> pointwise(a, &(fun.(&1, b)))
      is_number(a) -> pointwise(b, &(fun.(a, &1)))
      :else -> pointwise(a, b, fun)
    end
  end
  def pow(v, p), do: pointwise(v, &:math.pow(&1, p))
  def sq(v), do: pow(v, 2)

  def norm(v), do: V.div(v, mag(v))
  def mag(v), do: :math.sqrt(:math.pow(v.x, 2) + :math.pow(v.y, 2) + :math.pow(v.z, 2))

  def cross(a, b), do: %V{x: a.y*b.z - a.z*b.y, y: a.z*b.x - a.x*b.z, z: a.x*b.y - a.y*b.x}
  def dot(a, b), do: a.x*b.x + a.y*b.y + a.z*b.z

  def avg(list) do
    Enum.reduce(list, V.new, fn v, acc ->
      V.add(acc, v)
    end) |> V.div(length(list))
  end

  def export(v), do: [v.x, v.y, v.z]
end
