defmodule Mesh do
  require Integer
  alias VElixir.V

  defstruct name: nil, vertices: [], triangles: [], strips: [], fans: [], recently_added: nil
  def new(name), do: %Mesh{name: name}

  #new vertices must be at the end
  def push_vector(mesh, vec) do
    mesh
      |> Map.put(:recently_added, next_index(mesh))
      |> Map.update!(:vertices, &(&1 ++ [{vec, V.new}]))
  end
  def push_vectors(mesh, vecs) do
    start = next_index(mesh)
    mesh
      |> Map.update!(:vertices, &(&1 ++ Enum.map(vecs, fn vec -> {vec, V.new} end)))
      |> Map.put(:recently_added, Enum.to_list(start..(start + length(vecs) - 1)))
  end
  def next_index(mesh), do: length(mesh.vertices)

  def push_triangle(mesh, v1, v2, v3), do: push_attr(mesh, :triangles, [v1, v2, v3])
  def push_strip(mesh, strip), do: push_attr(mesh, :strips, strip)
  def push_strip(mesh, bottom_row, top_row, wrap \\ false) do
    strip = [a|[b|_]] = Enum.zip(bottom_row, top_row)
      |> Enum.map(fn {a, b} -> [a, b] end)
      |> List.flatten
    strip = if wrap, do: strip ++ [a, b], else: strip
    push_strip(mesh, strip)
  end
  def push_fan(mesh, fan), do: push_attr(mesh, :fans, fan)
  @doc "If wrap is true, the fan will auto wrap thereby connecting to itself to make a complete polygon. startI+1 will be appended to the fan list."
  def push_fan(mesh, startI, endI, wrap \\ true, reverse \\ false) do
    base = startI
    fan = Enum.to_list((startI+1)..endI)
    fan = if wrap, do: fan ++ [startI + 1], else: fan
    fan = if reverse, do: Enum.reverse(fan), else: fan
    push_fan(mesh, [base|fan])
  end

  def push_vector_ring(mesh, y, r, sliceCount) do
    dTheta = 2*:math.pi/sliceCount
    {vecs, _} = Enum.reduce(1..sliceCount, {[], 0}, fn _, {vecs, theta} ->
      {[V.new(r*:math.cos(theta), y, r*:math.sin(theta))|vecs], theta + dTheta}
    end)
    mesh
      |> push_vectors(vecs)
      |> Map.update!(:recently_added, fn recent=[h|_] -> recent ++ [h] end) #loop it
  end

  defp push_attr(mesh, key, data) do
    mesh
      |> Map.update!(key, &[data | &1])
      |> Map.put(:recently_added, data)
  end

  def add_normals(mesh) do
    mesh
      |> add_triangles_normals
      |> add_strips_normals
      |> add_fans_normals
  end

  defp add_triangles_normals(mesh) do
    Enum.reduce(mesh.triangles, mesh, fn [i1, i2, i3], mesh ->
      add_normal(mesh, i1, i2, i3)
    end)
  end

  defp add_strips_normals(mesh) do
    Enum.reduce(mesh.strips, mesh, fn strip, mesh ->
      add_strip_normals(mesh, strip)
    end)
  end
  defp add_strip_normals(mesh, strip, counter \\ 0)
  defp add_strip_normals(mesh, [i1|tail=[first|[second|_]]], counter) do
    {i2, i3} = if Integer.is_even(counter), do: {first, second}, else: {second, first}
    add_strip_normals(add_normal(mesh, i1, i2, i3), tail, counter + 1)
  end
  defp add_strip_normals(mesh, _, _), do: mesh

  defp add_fans_normals(mesh) do
    Enum.reduce(mesh.fans, mesh, fn [i1|fan], mesh ->
      add_fan_normals(mesh, i1, fan)
    end)
  end
  defp add_fan_normals(mesh, i1, [i2|tail=[i3|_]]) do
    add_fan_normals(add_normal(mesh, i1, i2, i3), i1, tail)
  end
  defp add_fan_normals(mesh, _i1, _), do: mesh

  defp add_normal(mesh, i1, i2, i3) do
    Map.update!(mesh, :vertices, fn vertices ->
      indices = [i1, i2, i3]
      verts = Enum.map(indices, &Enum.at(vertices, &1))
      [v1, v2, v3] = Enum.map(verts, fn {v, _n} -> v end)
      face_normal = V.cross(V.sub(v2, v1), V.sub(v2, v3))
      Enum.reduce(Enum.zip(indices, verts), vertices, fn {i, {v, n}}, vertices ->
        List.replace_at(vertices, i, {v, V.add(n, face_normal)})
      end)
    end)
  end

  @doc "Normalizes vertex normals and converts vertex tuples into lists."
  def export(mesh) do
    {vecs, normals} = Enum.reduce(Enum.reverse(mesh.vertices), {[], []}, fn {vec, normal}, {vecs, normals} ->
      {[V.export(vec)|vecs], [V.export(V.norm(normal))|normals]}
    end)
    Enum.each([mesh.triangles, mesh.strips, mesh.fans], fn feature_list ->
      Enum.each(feature_list, fn vec_list ->
        Enum.each(vec_list, fn vec_index ->
          if vec_index < 0 or vec_index >= length(vecs), do: raise "vec_index #{inspect(vec_index)} out of bounds."
        end)
      end)
    end)
    [vecs, normals, mesh.triangles, mesh.strips, mesh.fans]
  end

  def error_check(mesh) do
    Enum.reduce mesh.vertices, 0, fn {vec, normal}, i ->
      m = V.mag(normal)
      if m == 0, do: raise "vec #{inspect(i)} with value #{inspect(vec)} has a magnitude of #{inspect(m)}.\nMesh_data: #{inspect mesh}"
      i + 1
    end
  end
end
