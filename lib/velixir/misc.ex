defmodule VElixir.Misc do
  @doc "Works like Enum.map(list, fun), only it spawns a process for every element of the list."
  def async_map(list, fun) do
    list
      |> Enum.map(fn e -> Task.async(fn -> fun.(e) end) end)
      |> Enum.map(fn task -> Task.await(task, 120_000_000) end)
  end

  @doc "Works like Enum.map(list, fun) only it divides the work amongst `worker_count` processes."
  def worker_map(list, worker_count, fun) do
    {workers, []} = Enum.map_reduce 1..worker_count, list, fn workerNum, list ->
      {mine, rest} = Enum.split list, trunc(length(list)/(worker_count - workerNum + 1))
      {
        Task.async(fn ->
          Enum.map(mine, fun)
        end),
        rest
      }
    end
    Enum.flat_map(workers, fn worker -> Task.await(worker, 120_000_000) end)
  end
end
