defmodule VElixir.Arrow do
  use VElixir.Object
  alias VElixir.{V, Cylinder, Cone}

  def export(obj, mesh_map) do
    dir = Map.get(obj, :dir) || V.new(0, 1, 0)
    len = V.mag(dir)
    if len > 0 do
      pos = Map.get(obj, :pos) || V.new(0, 0, 0)
      color = Map.get(obj, :color, Color.new)
      r1 = 0.03*:math.sqrt(len)
      r2 = r1*2
      h = r2*2
      cyl = %{vtype: Cylinder, pos: pos, dir: dir, height: len - h, radius: r1, color: color}
      cone = %{vtype: Cone, pos: V.add(dir, V.mult(V.norm(dir), -h)), dir: dir, height: h, radius: r2, color: color}
      Enum.map [cyl, cone], fn obj -> obj.vtype.export(obj, mesh_map) end
    else
      []
    end
  end

  def build_mesh(), do: nil
end
