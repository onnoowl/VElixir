defmodule VElixir.Cylinder do
  use VElixir.Object
  alias VElixir.V

  def get_zoom(obj) do
    r = Map.get(obj, :radius)
    h = Map.get(obj, :height)
    cond do
      r || h ->
        r = r || 1
        Transform.scale(V.new(r, h || 1, r))
      :else ->
        nil
    end
  end

  def build_mesh(sliceCount \\ 30) do
    mesh = Mesh.new(__MODULE__)
      |> add_sides(sliceCount)
      |> add_face(0, sliceCount)
      |> add_face(1, sliceCount)
      |> Mesh.add_normals
    mesh
  end

  def add_face(mesh, y, sliceCount) do
    mesh = Mesh.push_vector_ring(mesh, y, 1, sliceCount)
    ring = mesh.recently_added
    center = Mesh.next_index(mesh)
    mesh
      |> Mesh.push_vector(V.new(0, y, 0))
      |> Mesh.push_fan([center | ring])
  end

  def add_sides(mesh, sliceCount) do
    mesh = Mesh.push_vector_ring(mesh, 0, 1, sliceCount)
    bottom = mesh.recently_added
    mesh = Mesh.push_vector_ring(mesh, 1, 1, sliceCount)
    top = mesh.recently_added
    Mesh.push_strip(mesh, bottom, top)
  end
end
