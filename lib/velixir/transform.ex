defmodule VElixir.Transform do
  alias VElixir.V
  alias VElixir.Matrix

  def apply_matrix(obj, trans) do
    if Map.get(obj, :__struct__) == V do
      Matrix.mult(trans, obj)
    else
      raise "Cannot apply matrix to the non vector object #{inspect(obj)}"
    end
  end

  def translate(renderable, vec) when is_map(renderable) do
    apply_matrix(renderable, translate(vec))
  end
  def translate(vec) do
    [
      [1, 0, 0, vec.x],
      [0, 1, 0, vec.y],
      [0, 0, 1, vec.z],
      [0, 0, 0, 1    ]
    ]
  end

  def rotate(renderable, angle, axis, about) when is_map(renderable) do
    apply_matrix(renderable, rotate(angle, axis, about))
  end
  def rotate(renderable, angle, axis) when is_map(renderable) do
    apply_matrix(renderable, rotate(angle, axis))
  end
  def rotate(angle, axis, about) do
    Matrix.mult([
      translate(about),
      rotate(angle, axis),
      translate(V.mult(about, -1))
    ])
  end
  def rotate(angle, axis) do
    u = V.norm(axis)
    s = :math.sin(angle)
    c = :math.cos(angle)
    [
      [c + sq(u.x)*(1-c),     u.x*u.y*(1-c) - u.z*s, u.x*u.z*(1-c) + u.y*s, 0],
      [u.y*u.x*(1-c) + u.z*s, c + sq(u.y)*(1-c),     u.y*u.z*(1-c) - u.x*s, 0],
      [u.z*u.x*(1-c) - u.y*s, u.z*u.y*(1-c) + u.x*s, c + sq(u.z)*(1-c),     0],
      [0,                     0,                     0,                     1]
    ]
  end

  def scale(size) do
    [
      [size.x, 0,      0,      0],
      [0,      size.y, 0,      0],
      [0,      0,      size.z, 0],
      [0,      0,      0,      1]
    ]
  end

  @doc "Assuming a non rotated object has a direction V.new(0, 1, 0), get_rotation_from_dir returns the rotation matrix that must be applied to it to have the direction dir."
  def get_rotation_from_dir(dir) do
    up = V.y
    cross = V.cross(up, dir)
    cross_mag = V.mag(cross)
    {angle, axis} = if cross_mag == 0 do
      IO.puts "cross_mag is zero."
      {:math.pi/2, V.x}
    else
      {:math.acos(V.dot(up, dir) / V.mag(dir)), cross}
    end
    rotate(angle, axis)
  end

  defp sq(n) do
    :math.pow(n, 2)
  end
end
