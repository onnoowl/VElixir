defmodule VElixir.Matrix do
  alias VElixir.V

  # @doc "nil can serve as an identity matrix."
  def mult(left, nil), do: left
  def mult(nil, right), do: right

  def mult(left, right) when is_map(right) do
    [[x], [y], [z], _] = mult(left, [[right.x], [right.y], [right.z], [(if right.is_norm, do: 0, else: 1)]])
    v = V.new(x, y, z)
    if right.is_norm, do: V.norm(v), else: v
  end
  def mult(left, right) do
    Enum.map(left, fn l_row ->
      Enum.map(0..(length(hd(right)) - 1), fn x ->
        r_col = Enum.map(right, fn r_row ->
          Enum.at(r_row, x)
        end)
        Enum.reduce(Enum.zip(l_row, r_col), 0, fn {l, r}, sum ->
          sum + l*r
        end)
      end)
    end)
  end
  def mult([h | t]) do
    Enum.reduce(t, h, fn e, acc ->
      mult(acc, e)
    end)
  end

  def export(matrix), do: List.flatten(matrix)
end
