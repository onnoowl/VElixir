defmodule VElixir.Cone do
  use VElixir.Object
  alias VElixir.V

  def get_zoom(obj), do: VElixir.Cylinder.get_zoom(obj)

  def build_mesh(sliceCount \\ 30) do
    mesh = Mesh.push_vector_ring(Mesh.new(__MODULE__), 0, 1, sliceCount)
    bot_ring = mesh.recently_added
    mesh = Mesh.push_vector_ring(mesh, 0, 1, sliceCount)
    top_ring = mesh.recently_added
    #bot_ring and top_ring are composed of the same vectors, but they are seperated so that the vectors don't add together.
    tip_vecs = Enum.map(1..sliceCount, fn _ -> V.new(0, 1, 0) end)
    mesh = Mesh.push_vectors(mesh, tip_vecs)
    tips = mesh.recently_added

    center = Mesh.next_index(mesh)

    mesh
      |> Mesh.push_vector(V.new(0, 0, 0))
      |> Mesh.push_fan([center | Enum.reverse(bot_ring)])
      |> Mesh.push_strip(top_ring, tips, true)
      |> Mesh.add_normals
  end
end
