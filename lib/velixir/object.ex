defmodule VElixir.Object do
  defmacro __using__(_) do
    quote location: :keep do
      alias VElixir.Transform
      alias VElixir.Matrix
      alias VElixir.Color

      def get_translation(obj) do
        pos = Map.get(obj, :pos)
        if pos do
          Transform.translate(pos)
        else
          nil
        end
      end

      def get_rotation(obj) do
        rotation = Map.get(obj, :rotation)
        dir = Map.get(obj, :dir)
        cond do
          rotation && dir ->
            raise "The properties `:rotation` and `:dir` cannot both be applied on the following object #{inspect(obj)}."
          rotation ->
            rotation
          dir ->
            Transform.get_rotation_from_dir(dir)
          :else ->
            nil
        end
      end

      def get_zoom(obj) do
        size = Map.get(obj, :size)
        cond do
          size ->
            Transform.scale(size)
          :else ->
            nil
        end
      end

      def get_transformation(obj) do
        Matrix.mult [
          Map.get(obj, :post_transform),
          get_translation(obj),
          get_rotation(obj),
          get_zoom(obj)
        ]
      end

      def get_color(obj) do
        Map.get(obj, :color) || Color.new(1.0, 1.0, 1.0)
      end

      def export(obj, mesh_map) do
        vtype_id = Map.get(mesh_map, obj.vtype)
        if !vtype_id, do: "Error, the vtype of `#{inspect(obj.vtype)}` is not registered. Here are all registered vtypes (mesh names): #{inspect(Map.keys(mesh_map))}"
        {vtype_id, Matrix.export(get_transformation(obj)), Color.export(get_color(obj))}
      end

      defoverridable [get_translation: 1, get_rotation: 1, get_zoom: 1, get_transformation: 1, export: 2]
    end
  end
end
