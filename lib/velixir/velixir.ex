defmodule VElixir do
  use GenServer

  @doc """
  opts:
  :call_update, default true. If call_update is true, the window will perform a cast with the tuple {:update, dt, t} before each render.
  :width, default 640. Window width.
  :height, default 480. Window height.
  """

  def start_link(args \\ %{}, opts \\ []) do
    {:ok, _} = Application.ensure_all_started(:velixir)
    args = if !Map.has_key?(args, :render_source), do: Map.put(args, :render_source, self()), else: args
    GenServer.start_link(__MODULE__, args, opts)
  end

  def standard_mesh_modules, do: [VElixir.Sphere, VElixir.Cylinder, VElixir.Cone]

  def init(args) do
    standard_meshes = Enum.map(standard_mesh_modules(), fn module -> module.build_mesh end)
    all_meshes = standard_meshes ++ Map.get(args, :meshes, [])
    Enum.each all_meshes, fn mesh ->
      Mesh.error_check(mesh)
    end

    # IO.puts "Launching cnano"
    {:ok, c_server} = :enm.pair
    :enm.connect(c_server, "ipc:///tmp/velixir.ipc")
    deps = Mix.Project.deps_paths()
    dir = if Map.has_key?(deps, :velixir) do
      Map.fetch!(deps, :velixir)
    else
      "."
    end
    port = Port.open({:spawn_executable, "#{dir}/cnano"}, [:exit_status, {:cd, dir}])
    Port.connect(port, self())
    {:ok, "start"} = :enm.recv(c_server, 1000) #wait for it to start

    state = %{
      mesh_counter: 0,
      mesh_map: %{},
      c_server: c_server,
    }
    state = Enum.reduce(all_meshes, state, fn mesh, state ->
      declare_mesh(state, mesh)
    end)

    {:ok, render_looper} = VElixir.RenderLooper.start_link(%{window: self(), c_server: c_server, opts: args})
    Process.flag(:trap_exit, true)
    {:ok, Map.put(state, :render_looper, render_looper)}
  end

  #External callback helpers

  def get_mesh_map(window) do
    GenServer.call(window, :get_mesh_map)
  end

  # Callbacks

  def handle_cast({:declare_mesh, mesh}, state) do
    {:noreply, declare_mesh(mesh, state)}
  end

  def handle_call(:get_mesh_map, _from, state) do
    {:reply, state.mesh_map, state}
  end

  def handle_info({_port, {:data, output}}, state) do
    IO.write "c_server: #{output}"
    {:noreply, state}
  end

  def handle_info({_port, {:exit_status, _status}}, state) do
    # IO.puts "Port close recieved"
    reason = {:shutdown, :window_closed}
    {:stop, reason, state}
  end

  def handle_info({:EXIT, pid, reason}, state) do
    if reason == :normal do
      {:noreply, state}
    else
      IO.puts "Captured exit from #{inspect(pid)}, reason #{inspect(reason)}"
      {:stop, reason, state}
    end
  end

  def terminate(_reason, state) do
    # IO.puts "TERMINATING window"
    :enm.send(state.c_server, <<0>>)
  end

  #Internal helpers

  defp declare_mesh(state, mesh) do
    id = state.mesh_counter
    send_data(state.c_server, :declare_mesh, [id | Mesh.export(mesh)])
    %{state|mesh_counter: state.mesh_counter + 1, mesh_map: Map.put(state.mesh_map, mesh.name, id)}
  end

  def send_data(c_server, type, data) do
    {:ok, encoded} = Msgpax.pack(data)
    num = case type do
      :quit -> 0
      :declare_mesh -> 1
      :transforms -> 2
      _ -> raise "Unknown type: #{inspect(type)}"
    end
    # if num < 3, do: IO.puts "#{inspect type} : #{inspect(data)}"
    :enm.send(c_server, [<<num>>|encoded])
  end

  #External helper functions

  def render(obj, acc \\ []) do
    if is_map(obj) and Map.has_key?(obj, :render) do
      Enum.reduce(obj.render, acc, fn key_set, acc ->
        key_set = if is_list(key_set), do: key_set, else: [key_set]
        render(get_in(obj, key_set), acc)
      end)
    else
      [obj | acc]
    end
  end
end
