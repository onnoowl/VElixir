defmodule VElixir.GenServer do
  defmacro __using__(_) do
    quote location: :keep do
      use GenServer

      use VElixir.V.Operators
      alias VElixir.{Transform, Matrix, Color, Sphere, Cylinder, Cone, Arrow}

      def handle_call(:render, _from, state) do
        acc = VElixir.render(state)
        {:reply, acc, state}
      end
    end
  end
end
