defmodule VElixir.V.Operators do
  defmacro __using__(_) do
    quote location: :keep do
      alias VElixir.V
      import VElixir.V, only: [norm: 1, mag: 1, cross: 2, dot: 2]

      import Kernel, except: [+: 2, -: 2, *: 2, /: 2]
      require VElixir.V.Operators
      import VElixir.V.Operators
    end
  end

  defmacro a + b do
    if is_number(a) and is_number(b) do
      quote do unquote(a) + unquote(b) end
    else
      quote do
        VElixir.V.add(unquote(a), unquote(b))
      end
    end
  end

  defmacro a - b do
    if is_number(a) and is_number(b) do
      quote do unquote(a) - unquote(b) end
    else
      quote do
        VElixir.V.sub(unquote(a), unquote(b))
      end
    end
  end

  defmacro a * b do
    if is_number(a) and is_number(b) do
      quote do unquote(a) * unquote(b) end
    else
      quote do
        VElixir.V.mult(unquote(a), unquote(b))
      end
    end
  end

  defmacro a / b do
    if is_number(a) and is_number(b) do
      quote do unquote(a) / unquote(b) end
    else
      quote do
        VElixir.V.div(unquote(a), unquote(b))
      end
    end
  end
end
