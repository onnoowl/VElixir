defmodule VElixir.Color do
  defstruct [:r, :g, :b, :a]

  @doc "r, g, b, and a should be between 0.0 and 1.0."
  def new(r, g, b, a \\ 1), do: %VElixir.Color{r: r, g: g, b: b, a: a}
  def new, do: new(1, 1, 1)

  def random do
    #try to generate  a darker value,   any value,  and a brighter value
    sd = 0.1
    vals = [abs(sd*:rand.normal), :rand.uniform, 1-abs(sd*:rand.normal)]
    [r, g, b] = Enum.shuffle(vals)
    new(r, g, b)
  end

  def export(c), do: [c.r, c.g, c.b, c.a]
end
