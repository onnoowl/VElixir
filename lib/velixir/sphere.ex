defmodule VElixir.Sphere do
  use VElixir.Object
  alias VElixir.V

  def get_zoom(obj) do
    r = Map.get(obj, :radius)
    cond do
      r ->
        Transform.scale(V.new(obj.radius, obj.radius, obj.radius))
      :else ->
        nil
    end
  end

  def build_mesh(sliceCount \\ 30, ringCount \\ 30) do
    dTheta = :math.pi/ringCount
    mesh = Mesh.new(__MODULE__)
      |> construct_cap(sliceCount, dTheta) #top cap
      |> construct_cap(sliceCount, :math.pi - dTheta) #bottom cap
    [[_|bottom_fan]|more_fans] = mesh.fans
    [[_|top_fan]|_] = more_fans
    top_fan = Enum.reverse(top_fan)

    # -1 to be exclusive, -2 to exlude the two caps made, and -1 to do the last strip manually = -4
    {mesh, prev, _} = Enum.reduce(0..(ringCount-4), {mesh, top_fan, dTheta*2}, fn _, {mesh, prev, theta} ->
      yOffset = :math.cos(theta)
      subR = :math.sin(theta)
      mesh = Mesh.push_vector_ring(mesh, yOffset, subR, sliceCount)
      ids = mesh.recently_added
      {Mesh.push_strip(mesh, ids, prev), ids, theta + dTheta}
    end)
    mesh = Mesh.push_strip(mesh, bottom_fan, prev)
    Mesh.add_normals(mesh)
  end

  # @doc "Adds a cap, as a fan, to the mesh."
  defp construct_cap(mesh, sliceCount, theta) do
    yOffset = :math.cos(theta)
    subR = :math.sin(theta)
    mesh = Mesh.push_vector_ring(mesh, yOffset, subR, sliceCount)
    ring = mesh.recently_added
    cap_point = Mesh.next_index(mesh)
    mesh
      |> Mesh.push_fan([cap_point|(if theta < :math.pi/2, do: Enum.reverse(ring), else: ring)])
      |> Mesh.push_vector(V.new(0, (if theta > :math.pi/2, do: -1, else: 1), 0)) #add base of fan, go 1 down from the origin.
  end
end
