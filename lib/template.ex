defmodule Template do
  use VElixir.GenServer

  def start_link(args, opts \\ []) do
    GenServer.start_link(__MODULE__, args, opts)
  end

  def init(_args) do
    {:ok, _} = VElixir.start_link

    state = %{
      objects: [], # declare your objects here
      render: [:objects]
    }

    {:ok, state}
  end

  def handle_cast({:update, _dt, _t}, state) do
    objects = Enum.map(state.objects, fn obj ->
      # modify your objects here
      obj
    end)
    {:noreply, %{state|objects: objects}}
  end
end

defmodule Mix.Tasks.Template do
  def run(args) do
    {:ok, _} = Template.start_link(args)
    # block until the program ends
    receive do
      {:stop, _reason} -> nil
    end
  end
end
