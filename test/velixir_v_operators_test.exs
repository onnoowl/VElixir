defmodule VElixirVOperatorsTest do
  use ExUnit.Case

  test "operator test" do
    use VElixir.V.Operators

    assert 2 + 3 == 5
    assert 4 * 3 == 12
    assert 7 - 2 == 5
    assert 20 / 5 == 4
    assert 1 / 4 == 0.25

    assert V.new(1, 2, 3) + V.new(2, 3, 4) == V.new(3, 5, 7)

    assert V.new(1, 2, 3) * 2 == V.new(2, 4, 6)
  end
end
