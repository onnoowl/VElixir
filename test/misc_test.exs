defmodule MiscTest do
  use ExUnit.Case
  doctest VElixir

  test "nested_update_reduce_with_key_sets" do
    obj = %{testing: [1, 2, 3, 4], major: %{minor: [5, 6, 7, 8], minor2: 5, minor3: %{a: 1, b: 2}, ignore: [:a, :b, :c]}, ignore: :a}
    {obj, acc} = VElixir.Misc.nested_update_reduce_with_key_sets(obj,
      [:testing, [:major, :minor], [:major, :minor2], [:major, :minor3]],
      0, fn num, acc ->
        {num + 1, acc + num}
      end
    )
    assert obj == %{ignore: :a, major: %{ignore: [:a, :b, :c], minor: [6, 7, 8, 9], minor2: 6, minor3: %{a: 2, b: 3}}, testing: [2, 3, 4, 5]}
    assert acc == 44
  end
end
