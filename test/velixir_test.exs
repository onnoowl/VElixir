defmodule VElixirTest do
  use ExUnit.Case
  doctest VElixir

  test "basic comms" do
    {:ok, data} = Msgpax.pack(["Greetings", 300, "Spartans"])
    {:ok, s} = :enm.pair
    :enm.connect(s, "ipc:///tmp/velixir.ipc")
    Enum.map 1..100, fn _ ->
      :enm.send(s, [<<2>>|data])
    end
    :enm.send(s, <<42>>)
  end
end
